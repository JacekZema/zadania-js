let controler = (()=>{
    class Account{
        constructor(balance, currency, number){
            this.balance = balance;
            this.currency = currency;
            this.number = number;
        }
    }
    class Person{
        constructor(firstName, lastName, accountsList){
            this.firstName = firstName;
            this.lastName = lastName;
            this.accountsList = accountsList;
        }
        findAccount(accountNumber){
            return this.accountsList.find((object) => object.number === accountNumber );
        }
        withdraw(accountNumber, amount){
            return new Promise((resolve, reject) =>{
                const foundAcc = this.findAccount(accountNumber) ;
                if ( (foundAcc && foundAcc.balance >= amount) && (amount > 0) ){
                    const countedMoney = foundAcc.balance - amount;
                    setTimeout( () => resolve(`Account number: ${foundAcc.number}, Withdrawn: ${amount}, New balance: ${foundAcc.balance = countedMoney} `) ,1000);
                }
                else if(!foundAcc)
                {
                    reject('This account does not exist');
                }
                else if(0 >= amount)
                {
                    reject(`You can't withdraw that money`);
                }
                else if(foundAcc.balance < amount)
                {
                    reject(`You don't have money on account`);
                }
                else{
                    reject('This account does not exist or some other problem?');
                }
            });
        }
    }

    const cardTitle = document.querySelector("h4.card-title");
    const cardText = document.querySelector("p.card-text");
    const fieldNumber = document.getElementById("number");
    const fieldAmount = document.getElementById("amount");
    const buttonSubmit = document.querySelector("button.btn.btn-primary");

    const januszNosacz = new Person('Janusz','Nosacz',[new Account(4040,'$',1),new Account(85858,'EURO',2)]);
    cardTitle.innerHTML = `${januszNosacz.firstName} ${januszNosacz.lastName}`;
    showAccInHTML(januszNosacz);

    function showAccInHTML (nameOfAcc){
        cardText.innerHTML = 'Accounts:' ;
        for(let acc of nameOfAcc.accountsList)
        {
            let accList = `Number: ${acc.number}, Balance: ${acc.balance}, Currency: ${acc.currency} `;
            cardText.innerHTML += "<p>" + accList + "</p>" ;
        }
    }
    return {
        withdrawHTML: function (){
            let accountNumber = parseInt(document.getElementById('number').value);
            let howMoney = parseFloat(document.getElementById('amount').value);
            januszNosacz.withdraw(accountNumber, howMoney)
                .then((res)=> alert(res))
                .then(()=> showAccInHTML(januszNosacz))
                .catch((err)=>alert(err));
        },
        onChanged: function(){
                buttonSubmit.disabled = !(fieldNumber.value.length > 0 && fieldAmount.value.length > 0 );
        }
    }
})();



