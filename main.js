function personFactory(){
    let details = {
        firstName: 'John',
        lastName: 'Dir',
        accountsList: [
            one = {
                balance: 12,
                currency: '$'
            },
            two = {
                balance: 23,
                currency: 'EURO'
            },
            tree = {
                balance: 34,
                currency: 'PLN'
            }
        ]
    };
    return {
            firstName: details.firstName,
            lastName: details.lastName,
            sayHello: function()  {
                return 'First name: ' + this.firstName + ', ' + 'Last name: ' + this.lastName + ', ' + 'Amount of acc: ' + details.accountsList.length;
        }
    };

}

nowyObiekt = personFactory();
//console.log(nowyObiekt);
console.log(nowyObiekt.sayHello());



