var Person = (function (){
    let details = {
        firstName: 'Marian',
        lastName: 'Dir'
    };

    let accountsList = [
        one = {
            balance: 12,
            currency: '$'
        },
        two = {
            balance: 23,
            currency: 'EURO'
        },
        tree = {
            balance: 34,
            currency: 'PLN'
        }
    ];

    function calculateBalance() {
        let totalMoney = 0;
        for(let i = 0; i < accountsList.length; i++)
        {
            totalMoney += accountsList[i].balance;
        }
        return totalMoney;
    }

    return {
            firstName: details.firstName,
            lastName: details.lastName,
            sayHello: function()  {
            return 'First name: ' + this.firstName + ', ' + 'Last name: ' + this.lastName + ', ' + 'Amount of acc: ' + accountsList.length + ', Balance: ' + calculateBalance();
        },
            addAccount: (addMoney, addCurrency) => {
               return accountsList.push({"balance":addMoney, "currency":addCurrency})
        }
    };

})();

console.log(Person.sayHello());
Person.addAccount(5,'EURO');
console.log(Person.sayHello());




