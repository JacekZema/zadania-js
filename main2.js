var Account = function(addBalance, addCurrency){
    this.balance = addBalance;
    this.currency = addCurrency;
};

var Person = (function (){
    let details = {
        firstName: 'Marian',
        lastName: 'Dir'
    };

    let accountsList = [];

    function calculateBalance() {
        let totalMoney = 0;
        for(let i = 0; i < accountsList.length; i++)
        {
            totalMoney += accountsList[i].balance;
        }
        return totalMoney;
    }

    return {
            firstName: details.firstName,
            lastName: details.lastName,
            sayHello: function() {
                return 'First name: ' + this.firstName + ', ' + 'Last name: ' + this.lastName + ', Amount of acc: ' + accountsList.length + ', Balance: ' + calculateBalance();
            },
            addAccount: (addBalance, addCurrency) => {
               return accountsList.push(new Account(addBalance,addCurrency))
        }
    };
})();


console.log(Person.sayHello());
Person.addAccount(43,'EURO');
Person.addAccount(4456,'EURO');
Person.addAccount(34,'EURO');
console.log(Person.sayHello());




