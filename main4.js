class Account{
    constructor(balance, currency, number){
        this.balance = balance;
        this.currency = currency;
        this.number = number;
    }
}

class Person{
    constructor(firstName, lastName, accountsList){
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountsList = accountsList;
    }
    _calculateBalance() {
        let totalMoney = 0;
        for(let account of this.accountsList)
        {
            totalMoney += account.balance;
        }
        return totalMoney;
    }

    addAccount(addBalance, addCurrency, addNumber){
        return this.accountsList.push(new Account(addBalance,addCurrency,addNumber))
    }
    sayHello() {
        return `First name: ${this.firstName}, Last name: ${this.lastName}, Amount of acc: ${this.accountsList.length}, Total balance: ${this._calculateBalance()}`
    }
    filterPositiveAccounts(){
        return this.accountsList.filter((elem) => {
            return elem.balance > 0
        });
    }
    findAccount(accountNumber){
      return this.accountsList.find((object) => object.number === accountNumber );
    }
    withdraw(accountNumber, amount){
        return new Promise((resolve, reject) =>{
            const foundAcc = this.findAccount(accountNumber) ;
            if ( foundAcc && foundAcc.balance >= amount ){
                const countedMoney = foundAcc.balance - amount;
                setTimeout( () => resolve(`Account number: ${foundAcc.number}, Withdrawn: ${amount}, New balance: ${foundAcc.balance = countedMoney} `) ,3000);
            }
            else if(!foundAcc)
            {
                reject('This account not exist');
            }
            else if(foundAcc.balance < amount)
            {
                reject('You dont have money on account');
            }
            else{
                reject('This account not exist or any other problem?');
            }
        });
    }
}

const kowalski = new Person('Janusz','Nosacz',[new Account(4040,'$',1),new Account(85858,'EURO',2)]);
console.log(kowalski.sayHello());
console.log(kowalski.filterPositiveAccounts());
console.log(kowalski.addAccount(484848484,'$',3));
console.log(kowalski.accountsList);
kowalski.withdraw(1,0)
    .then((success)=> console.log(success))
    .catch((err)=>console.log(err));

//setTimeout( () => console.log(kowalski.accountsList), 3000);


